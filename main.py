from pathlib import Path
import argparse
from bs4 import BeautifulSoup
import requests
import re
from pytube import YouTube
import pydub
from time import sleep
parser = argparse.ArgumentParser()
parser.add_argument("url", type=str)
parser.add_argument("--output", default="./", type=str)
parser.add_argument("--verbosity-level", default=1, type=int)
args = vars(parser.parse_args())

url = args['url']
output = args['output']
verbosity_level = args['verbosity_level']

VERBOSITY_NONE  = 0
VERBOSITY_INFO  = 1
VERBOSITY_DEBUG = 2

def get_tracklist(url, info_level=False):
    if info_level:
        print("Requesting tracklist...")
    source = requests.get(url).text
    soup = BeautifulSoup(source, 'html5lib')
    tracklist = None
    attempt = 0
    #Sometimes Beautiful soup fails to retrieve data, so we need to retry
    while not tracklist and attempt < 5:
        try:
            description = soup.find('div', id='watch-description-text')
            for br in description.find_all("br"):
                br.replace_with("\n")
            tracklist = re.findall(r'((?:[0-9]{2}:)+[0-9]{2})[^A-Za-zÀ-ÖØ-öø-ÿ0-9]*(.*)', description.get_text())
        except:
            pass
        attempt += 1
        if not tracklist:
            if info_level:
                print(f"Failed to retrieve tracklist data, retrying... (attempt #{attempt})")
            sleep(3)
    if not tracklist:
        raise Exception("Couldn't retrieve tracklist")
    for k in range(len(tracklist)):
        tracklist[k] = list(tracklist[k])
        times = tracklist[k][0].split(":")
        time = 0
        for i in times:
            time *= 60
            time += int(i)
        tracklist[k][0] = time*1000
        tracklist[k] = tuple(tracklist[k])
    return tracklist

def download_audio(url, path, info_level=False):
    if info_level:
        print("Starting downloading video process...")
    video = YouTube(url)
    if info_level >= VERBOSITY_DEBUG:
        print(f"Audio formats: {video.streams.filter(only_audio=True)}")
        print(f"Saving path: {path}")
    stream = ""
    try:
        stream = video.streams.filter(only_audio=True, subtype='mp4')[0]
    except:
        stream = video.streams.filter(only_audio=True)[0]
    if stream:
        stream.download(output_path=path.parent, filename=path.name)
    else:
        raise Exception("No audio source found")

def split_audio(source, output_path, tracklist, metadata=None, input_format="mp3", output_format="mp3", info_level=False):
    if info_level:
        print("Splitting into multiple audio files...")
        if info_level >= VERBOSITY_DEBUG:
            print(f"Source file: {str(source)}")
    if metadata is None:
        metadata = [None]*len(tracklist)
    audio_file = pydub.AudioSegment.from_file(str(source), format=input_format)
    tracklist.reverse()
    stop = len(audio_file)
    for k in range(len(tracklist)):
        sequence = tracklist[k]
        if info_level >= VERBOSITY_INFO:
            print(f"Splitting audio sequence #{k} - {sequence[1]}")
        audio_file[sequence[0]:stop].export(
            f'{output_path}/{sequence[1]}.{output_format}',
            format=output_format,
            tags=metadata[k]
        )
        stop = sequence[0]

# Get working directory and creating tmp directory
working_path = Path(output)
tmp_path = working_path / "tmp"
tmp_path.mkdir(parents=True, exist_ok=True)

# Creating audio source path
filename = str(abs(hash(url)))
sourcefile = tmp_path / filename

# Downloading audio
download_audio(url, sourcefile, info_level=verbosity_level)

# Adjust audio file path because pytube absolutly want to save file with extension
sourcefile = list(tmp_path.glob(f"{filename}.*"))[0]
extension = sourcefile.suffix[1:]

# Getting tracklist
tracklist = get_tracklist(url, info_level=verbosity_level)

# Splitting audio source file according to tracklist
split_audio(sourcefile, working_path, tracklist, input_format=extension, info_level=verbosity_level)

sourcefile.unlink()
tmp_path.rmdir()